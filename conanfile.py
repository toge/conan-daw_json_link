from conans.errors import ConanInvalidConfiguration
from conans import ConanFile, CMake, tools
import os

class DawJsonLinkConan(ConanFile):
    name = "daw_json_link"
    license = "BSL-1.0"
    author = "toge.mail@gmail.com"
    url = "https://bitbucket.org/toge/conan-daw_json_link"
    homepage = "https://github.com/beached/daw_json_link"
    description = "Static JSON parsing in C++ "
    topics = ("json", "parse", "json-parser", "serialization", "constexpr")
    settings = "os", "compiler"
    generators = "cmake"

    _compiler_required_cpp17 = {
        "gcc": "7",
        "clang": "5",
        "Visual Studio": "15",
        "apple-clang": "10",
    }

    def configure(self):
        if self.settings.get_safe("compiler.cppstd"):
            tools.check_min_cppstd(self, "17")
        try:
            minimum_required_compiler_version = self._compiler_required_cpp17[str(self.settings.compiler)]
            if tools.Version(self.settings.compiler.version) < minimum_required_compiler_version:
                raise ConanInvalidConfiguration("This package requires c++17 support. The current compiler does not support it.")
        except KeyError:
            self.output.warn("This recipe has no support for the current compiler. Please consider adding it.")

    def source(self):
        tools.get(**self.conan_data["sources"][self.version])
        os.rename("daw_json_link-{}".format(self.version), "daw_json_link")

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.configure(source_dir = "daw_json_link")
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()
        self.copy("*.h", dst="include/third_party", src="daw_json_link/include/third_party")

    def package_id(self):
        self.info.header_only()